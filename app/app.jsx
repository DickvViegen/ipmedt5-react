var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');
var Main = require('Main');
var PopularProducts = require('PopularProducts');
var Product = require('Product');

// Load Foundation
require('style!css!foundation-sites/dist/foundation.min.css')
// require('style!css!react-bootstrap/dist/bootstrap.min.css')
// App CSS
require('style!css!sass!applicationStyles')
$(document).foundation();

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/" component={Main}>
			<IndexRoute component={PopularProducts}/>
			<Route path="/product" component={Product}/>
		</Route>

	</Router>,
	document.getElementById('app')
);
