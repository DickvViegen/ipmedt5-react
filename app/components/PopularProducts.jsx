var React = require('react');
var ReactDOM = require('react-dom');

var PopularProducts = React.createClass({
  render: function () {
    return (
      <div>
        <div className="expanded row">
          <div className="expanded row ">
            <div className="columns small-12 text-capitalize title">
              <h1 className="text-center">Populaire Dranken</h1>
            </div>
          </div>
        </div>
        <div className="expanded row">
          <div className="drink columns small-12 popular-padding even">
            <div className="columns small-3">
              <img className="img-circle center" src="http://lorempixel.com/160/160/food"></img>
            </div>
            <div className="columns small-9">
              <h2>Dranknaam</h2>
              <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit</h4>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = PopularProducts;
