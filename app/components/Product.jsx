var React = require('react');
var ReactDOM = require('react-dom');
var LaravelAPI = require('LaravelAPI');

var Product = React.createClass({
  getInitialState: function () {
    return {
      isLoading: false,
    }
  },
  componentDidMount: function() {
    var that = this;

    this.setState({
      isLoading: true,
      name: undefined,
      temp: undefined
    });
    // console.log(LaravelAPI.getProduct(1));
    LaravelAPI.getProduct(1).then(function(drink){
      // console.log(drink);
      that.setState({
        isLoading: false,
        name: drink['name'],
        brand: drink['brand'],
        characteristic: drink['characteristic'],
        country: drink['country'],
        description: drink['description'],
        region: drink['region'],
        year: drink['year']
      });
    }, function (e) {
      that.setState({
        isLoading: false,
        errorMessage: e.message
      })
    });
  },
  render: function() {
    var {name, brand, characteristic, country, description, region, year} = this.state;
    return (
      <div className="expanded row">
        <div className="text-center">
          <div className="row expanded drink-header">
            <div className="drink-image">
              <img className="img-circle align-center" src="http://lorempixel.com/300/300/food"></img>
            </div>
            <div className="drink-title columns small-12">
              <h1>{name}</h1>
            </div>
            <div className="drink-prijs columns small-12">
              <h2>&euro;69,69</h2>
            </div>
          </div>
          <div className="expanded row">
            <div className="description columns small-12 even">
              <h2>Omschrijving</h2>
              <p>
                {description}
              </p>
            </div>
            <div className="origin columns small-12 uneven">
              <h2>Herkomst</h2>
              <ul>
                <li>Land: {country}</li>
                <li>Regio: {region}</li>
                <li>Jaar: {year}</li>
              </ul>
            </div>
            <div className="characteristic columns small-12 even">
              <h2>Karakteristiek</h2>
              <ul>
                <li>{characteristic}</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
              </ul>
            </div>
            <div className="best-with columns small-12 uneven">
              <h2>Het beste bij</h2>
              <ul>
                <li>Gelegenheid</li>
                <li>Gerecht</li>
                <li>Gerecht</li>
                <li>Seizoen</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = Product;
